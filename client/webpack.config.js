const CopyPlugin = require('copy-webpack-plugin');
const path = require('path');

const public_dist = path.resolve(__dirname, '../dist/public');

module.exports = {
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    hot: true,
    port: 3000,
  },
  entry: path.resolve(__dirname, './js/index.js'),
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: "babel-loader",
        options: {
          presets: [
            "@babel/preset-env",
            "@babel/preset-react"
          ],
        },
      }
    ]
  },
  output: {
    path: public_dist,
  },
  plugins: [
    new CopyPlugin([
        { from: path.resolve(__dirname, 'html'), to: public_dist },
        { from: path.resolve(__dirname, 'css'), to: public_dist },
        { from: path.resolve(__dirname, 'images'), to: public_dist },
      ],
    ),
  ],
};
